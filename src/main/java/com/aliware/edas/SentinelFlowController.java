package com.aliware.edas;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;

@RestController
public class SentinelFlowController {

    @GetMapping("/flow")
    public String flow() {
        return "success";
    }

    @GetMapping("/save")
    @SentinelResource(value = "ceshi", blockHandler = "block")
    public String save() {
        return "test";
    }

    public String block(BlockException e) {
        return "block";
    }

}          